package net.jxvtc.eshop.utils;
/**
 * 数据源的支持类
 * @author USER
 *
 */

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DataSourceUtils2 {
	private static DataSource dataSource = new ComboPooledDataSource();
	private static Connection conn ;

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException
	{
		
		// 
		if(conn==null)
		{
			conn=dataSource.getConnection();
			
			
		}
		return conn;
	}
	
	public static void startTransaction() throws SQLException {
		
		if(conn!=null)
		{
			conn.setAutoCommit(false);
		}
	}
	
	public static void rollback() throws SQLException {
		
		if(conn!=null)
		{
			conn.rollback();
		}
	}
	
	public static void releaseAndCloseConnection() throws SQLException
	{
		
		if(conn!=null)
		{
			conn.commit();
		
			conn.close();
		}
	}
	
	
}
